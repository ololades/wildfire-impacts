# Wildfire Impacts



## Project brief
The city state want to identify how much land has been impacted by wildfires over the past 10 years for a report summarizing impacts for each County in California. The task includes; 
- calculate the total area burned by wildfires in each County 
- summarize the broader patterns of wildfires throughout California
- Generate a map showing the affected areas in percentage

## Analysis Steps
- Create a new ArcGIS Pro Project (using the “map” template) and add the data to the map.
- Conduct your analysis:
    * Dissolve the wildfire perimeters into a single feature using the dissolve tool.
    * Intersect the dissolved wildfire perimeters and the county layer as your using the intersect tool.
    * In the results of the intersect tool add a new field to the attribute table to store the area (in acres) of polygon. Use the calculate geometry tool in the attribute table to calculate the area (in US Survey Acres) for each of the features.
    * Use the Summary Statistics tool to calculate to sum the area of wildfire areas by county. Set the “COUNTY” field as the case field and sum the values from the field you added to the intersect tool output.
    * Add a new field to the County layer to store the area from the wildfire summary table.
    * Join the output of the Summary Statistics tool to the County layer
Use the Calculate Field too in the County attribute table to update the new empty field with the area values from the Summary Statistics tool output.
    * Remove the join between the County layer and the Summary Statistics output.
    * Add a new field to the County Layer and calculate the percentage of areas burned by wildfire to the total area of each county. Be sure to multiple the value by 100 so the values are represented as a percentage. Here is an example of what the expression should look like (!Wildfire_Area!/!Area_Acres!)*100, with ‘Wildfire_Area’ being the field that contains the wildfire area you calculated and ‘Area_Acres’ being the area field (in acres) from the County layer.
- Symbolize the counties by the percentage of area burned by wildfires. You can choose any color ramp that you like, but use just 5 classes and use the default classification method of Natural Breaks (Jenks) to have your map be comparable. The general trend should look similar to he image below, although your colors maybe different.
- Make an appropriate map layout with a title, a legend with information about the symbol classes (just the range of percentages within each class if fine), a list of data sources, your name, the date you published the map, a scale bar, a north arrow, and the map itself. Make sure to include a basemap for reference. Additionally, the counties should be labeled with their names (just the name is sufficient, you do not need to add the word “County” at the end as in the lecture on labeling).
- Add metadata to your final county layer indicate the geoprocessing workflow you conducted on the data. Make sure to indicate your name, the source data involved, where the source data was obtained, and the date you did the processing and relevant details for what you did to generate the layer. For this project use the default Item Description format for the metadata.
- Export a PDF of the map’s layout and a map package of the map.


## Things I learnt
- Data Integration and Analysis: Learned to integrate wildfire perimeters and county data layers to analyze wildfire impact by county area.
- Geoprocessing Tools: Utilized tools like Dissolve, Intersect, Calculate Geometry, and Summary Statistics to manipulate and analyze spatial data.
- Spatial Joins and Attribute Updates: Performed a spatial join to connect wildfire area data with county data and updated county attributes with wildfire impact calculations.
- Map Symbology and Classification: Created a thematic map using Natural Breaks classification to symbolize counties based on the percentage area burned by wildfires.
- Map Layout and Documentation: Designed a map layout with title, legend, data sources, metadata, scale bar, and north arrow for effective communication of map findings.